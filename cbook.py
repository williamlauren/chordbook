import sys
import os
import pandas as pd
import fire

from song import Song
from urlparser import get_ug_url, normalize

user = os.path.expanduser('~').split(os.path.sep)[-1]

if user == 'willlaur':
    sys.path.append(
        r'C:\Users\willlaur\OneDrive - Citecgroup\docs\misc\scripts')
else:
    sys.path.append(r'C:\Users\X260\docs\scripts')

from funcs.io import load_obj, save_obj


def get_spotify_songs(path):

    df = pd.read_excel(path, header=None)
    df.columns = ['COL']

    df['Title'], df['Artist'] = df['COL'].str.split(' – ', 1).str
    df.drop('COL', axis=1, inplace=True)

    df['Title'] = df.Title.apply(
        lambda x: x.split('-')[0].strip() if '-' in x else x.strip())

    df['Artist'] = df.Artist.apply(
        lambda x: x.split(',')[0].strip() if ',' in x else x.strip())

    popular_artists = df[['Artist', 'Title']].groupby(
        by='Artist').count().sort_values(by='Title', ascending=False).index.tolist()

    popular_artists = {n: i for i, n in enumerate(popular_artists, start=1)}
    popular_artists

    df['Rank'] = df.Artist.map(popular_artists)

    df.sort_values(by='Rank', inplace=True)

    return df


def append_txt(row, fname, rm=False):

    if rm:
        if os.path.isfile(fname):
            os.remove(fname)

    with open(fname, 'a') as f:
        try:
            f.write(';'.join(map(str, row)) + '\n')
        except:
            row = list(row)
            for i in [0, 1]:
                row[i] = normalize(row[i])

            f.write(';'.join(map(str, row)) + '\n')


def load_txt(fname):

    if not os.path.isfile(fname):
        return None

    ret = []
    with open(fname, 'r') as f:
        for n in f.read().split('\n'):
            if len(n):
                ret.append(n.split(';'))

    return ret


def get_urls(df):
    """
    df needs to have Title and Artist columns
    """

    urls = load_txt('data/urls.txt')

    if urls is None:
        found = set()
    else:
        found = set((artist, title) for artist, title, url in urls)

    print(f'urls.txt contains {len(found)} rows')

    N = len(df) - len(found)
    i = 0

    # start at the end
    for artist, title in list(zip(df.Artist, df.Title))[::-1]:

        if (artist, title) in found:
            continue

        i += 1

        try:
            url = get_ug_url(artist, title)

        except Exception as e:
            print(f'\nException: {e}\nfor {artist} - {title}')
            url = None

        if url is not None:

            append_txt((artist, title, url), 'data/urls.txt')
            found.update((artist, title))

            print(f'{round(100 * i / N, 1): < 10}{artist} - {title}: {url.replace("https://tabs.ultimate-guitar.com/tab/", "")}')

        else:
            print(f'{round(100 * i / N, 1): < 10}{artist} - {title}: None')

    print('done')


def get_songs(data):
    """
    data is dict with key: (title, artist) tuple and value: url string
    """

    if os.path.isfile('data/songs.pkl'):
        songs = load_obj('data/songs.pkl')
    else:
        songs = []

    found = set((song.artist, song.title) for song in songs)
    print(f'songs.pkl contains {len(found)} songs')

    N = len(data) - len(found)
    i = 0

    try:

        for artist, title, url in data:

            if (artist, title) in found:
                continue

            try:
                song = Song(url, artist=artist, title=title)

            except Exception as e:
                print(f'\nException: {e}\nfor {artist} - {title}')
                song = None

            if song is not None:
                songs += [song]
                print(f'{round(100 * i / N, 1):<10}{artist} - {title} \t {i}')

                found.update((artist, title))

            i += 1

            if i % 30 == 0:
                save_obj(songs, 'data/songs.pkl')
                print('\tsaving songs.pkl')

    except KeyboardInterrupt:
        print('interrupted')

    finally:
        save_obj(songs, 'data/songs.pkl')

    print('done')

    return songs


def save_raw_data(songs):

    rdata = {}

    for song in songs:

        rdata[(song.artist, song.title)] = (song.url, song.chord_str)

    save_obj(rdata, 'data/raw-chord-strings.pkl')


def rebuild_songs(rdata):

    songs = []

    for artist, title in rdata:
        url, chord_str = rdata[(artist, title)]

        song = Song(url=url, chord_str=chord_str,
                    title=title, artist=artist)

        songs += [song]

    save_obj(songs, 'data/songs.pkl')

    print('done')


def main(cmd):

    cmd = cmd.strip().lower()

    if cmd == 'url':

        data = load_txt('data/album-songs.txt')
        df = pd.DataFrame(data, columns=['Artist', 'Album', 'Title'])
        get_urls(df)

    elif cmd == 'songs':
        urls = load_txt('data/urls.txt')
        songs = get_songs(urls)
        save_raw_data(songs)

    elif cmd == 'rebuild':

        rdata = load_obj('data/raw-chord-strings.pkl')
        rebuild_songs(rdata)

    else:
        print(f'unknown command: {cmd}')


if __name__ == '__main__':

    fire.Fire(main)

    # TODO: https://github.com/kennethreitz/pytheory use this maybe?
