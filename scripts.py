from urlparser import normalize
from bs4 import BeautifulSoup
import requests
import re
from cbook import append_txt, load_txt


def get_response(url):

    try:
        page = requests.get(url)
    except Exception as e:
        print(e)
        return None, None
    
    html = page.text

    soup = BeautifulSoup(html, 'lxml')


    string = soup.prettify()

    return soup, string


def get_songs(artist, album):

    artist = normalize(artist)
    album = normalize(album)

    query = f'{artist.lower()} {album.lower()} site:genius.com'

    url1 = f'https://www.google.fi/search?q={query.replace(" ", "+")}'

    _, string = get_response(url1)

    if string is None:
        return []
    
    if 'CAPTCHA' in string:
        print('CAPTCHA required for Google')
        return []

    pattern = r'https://genius.com/albums(.+?)\&'

    match = re.search(pattern, string)

    if match is not None:
        url2 = match.group(0)[:-1]

    else:
        return []

    soup, _ = get_response(url2)

    if soup is None:
        return []

    songs = []

    for hit in soup.findAll(attrs={'class': 'chart_row-content-title'})[:-1]:
        hit = hit.text.strip()
        song = hit.split('\n')[0].replace('\xa0', ' ').strip()

        if '(Ft.' in song:
            song = song[:song.index('(Ft.')]

        if 'Ft.' in song:
            song = song[:song.index('Ft.')].strip()

        songs += [song]

    return songs

def get_album_songs():

    dfa = pd.read_csv('data/RYM-albums.txt', delimiter=';')



    album_songs = load_txt('data/album-songs.txt')

    found = {(n[0], n[1]) for n in album_songs}

    for i, row in dfa.iterrows():
        artist, album = row.artist, row.album

        if (artist, album) in found:
            continue
        print(artist, album)
        songs = get_songs(artist, album)

        if len(songs):
            print(f'{album} has {len(songs)} songs')
            for song in songs:
                row = [artist, album, song]
                append_txt(row, 'data/album-songs.txt')