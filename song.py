import re
from chord import Chord
from urlparser import parse_url


def parse_chords(chord_str, steps, preference=None):

    pattern = r'\[ch\](.+?)\[\\/ch\]'
    final_str = chord_str
    offset = 0
    chord_list = []

    interactive = True

    positions = list(re.finditer(pattern, chord_str))
    if len(positions):

        for m in positions:
            i, j = m.start(0), m.end(0)

            try:
                ch = Chord(chord_str[i + 4: j - 6],
                           preference=preference).transpose(steps)
            except ValueError:
                ch = '[?]'

            chord_list += [ch]

            final_str = final_str[:i - offset] + \
                str(ch) + final_str[j - offset:]
            offset += j - i - len(str(ch))

    else:
        # TODO: parse chords that aren't in [ch]..[\ch] format
        interactive = False

    final_str = '\n'.join(final_str.replace(
        '\\r', '').replace('\\t', '\t').split('\\n'))

    final_str = final_str.replace(r'\/', '/').replace(r'\\', '/')

    return final_str, chord_list, interactive


class Song:

    def __init__(self, url=None, chord_str=None,
                 title='Unknown title', artist='Unknown artist'):

        self.url = url
        self.title = title
        self.artist = artist

        if chord_str is None:
            self.chord_str = parse_url(url)
        else:
            self.chord_str = chord_str

        self.chords, self.chord_list, self.interactive = parse_chords(
            self.chord_str, steps=0)

        self.key = str(self.chord_list[0]) if len(self.chord_list) else None
        self.orig_key = self.key

        self.transposed = 0

    def __str__(self):
        string = (
            f'{self.artist} - {self.title}{" (NOT POSSIBLE TO TRANSPOSE)" if not self.interactive else ""}'
            f'\nkey: {self.key}')

        if self.interactive:
            string += f' (original key {self.orig_key}, transposed: {"+" if self.transposed > 0 else ""}{self.transposed})'

        string += f'\n\n{self.chords}'
        
        return string

    def __repr__(self):
        return self.__str__()

    def transpose(self, steps, preference=None):

        self.transposed = steps

        self.chords, self.chord_list, self.interactive = parse_chords(
            self.chord_str, steps=self.transposed, preference=preference)

        self.key = str(self.chord_list[0]) if len(self.chord_list) else None
        return self

    def set_preference(self, preference):
        return self.transpose(self.transposed, preference=preference)
