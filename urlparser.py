import urllib.request
from bs4 import BeautifulSoup
import re
import unicodedata
import requests
import time


def normalize(x):

    x = ''.join(filter(lambda x: str.isalnum(x) or ' ' in x, x))
    bytestr = unicodedata.normalize('NFKD', x).encode('ASCII', 'ignore')
    string = "".join(map(chr, bytestr))

    return string


def get_ug_url(artist, title):

    artist = normalize(artist)
    title = normalize(title)

    query = f'{artist.lower()} {title.lower()} chords'

    url = f'https://www.google.fi/search?q={query.replace(" ", "+")}'

    # hdr = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'}
    # req = urllib.request.Request(url, headers=hdr)
    # with urllib.request.urlopen(req) as response:
    # html = response.read()

    page = requests.get(url)
    html = page.text

    soup = BeautifulSoup(html, 'lxml')

    pattern = r'https://tabs.ultimate-guitar.com(.+?)\&'

    string = soup.prettify()

    match = re.search(pattern, string)

    if match is not None:

        url = match.group(0)[:-1]
        return url
    else:
        if 'CAPTCHA' in string:
            print('CAPTCHA required')


def parse_ug(soup):
    scripts = soup.find_all('script')

    for scr in scripts:
        scr_text = scr.text.strip().replace(r'\"', '')

        if not len(scr_text):
            continue

        match = re.search(r'{\"content\":\"(.+?)\"', scr_text)

        if match is None:
            continue

        chords = match.group(1)
        return chords


def parse_chordie(soup):
    raise NotImplementedError('parser for chordie not available')


def parse_echords(soup):
    raise NotImplementedError('parser for e-chords not available')


def parse_url(url):

    with urllib.request.urlopen(url) as response:
        html = response.read()

    soup = BeautifulSoup(html, 'lxml')

    if 'ultimate-guitar' in url:
        return parse_ug(soup)

    elif 'chordie' in url:
        return parse_chordie(soup)

    elif 'e-chords' in url:
        return parse_echords(soup)

    else:
        raise NotImplementedError(f'no parser found for {url}')
