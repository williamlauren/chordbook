# Chordbook

* Parse chords and lyrics from url
    * Supports ``ultimate-guitar.com``, ``chordie.com``
* View chords and transpose
* Save data as JSON or SQLite database file?
