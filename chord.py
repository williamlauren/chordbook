class Chord:

    KEYS = [
        ('A',),
        ('A#', 'Bb'),
        ('B',),
        ('C',),
        ('C#', 'Db'),
        ('D',),
        ('D#', 'Eb'),
        ('E',),
        ('F',),
        ('F#', 'Gb'),
        ('G',),
        ('G#', 'Ab')
    ]

    # chord modifers grouped depending on whether they are considerer major or minor
    # tuple order should be so that str.startswith() can be used in a loop over the tuples
    # for maj_mod, min_mod in zip(major_modifiers, minor_modifiers)
    MODIFIERS = {
        'major': ('sus2', 'sus4', 'sus', None, 'maj7', None, None, 'maj', '5', '6', '7', '9', '13', None),
        'minor': (None, None, None, 'min6', 'min7', 'min9', 'min13', 'min', None, 'm6', 'm7', 'm9', 'm13', 'm')
    }

    assert len(MODIFIERS['major']) == len(MODIFIERS['minor'])

    KEY_IDX_MAP = {
        'A': 0,
        'A#': 1,
        'Bb': 1,
        'B': 2,
        'C': 3,
        'C#': 4,
        'Db': 4,
        'D': 5,
        'D#': 6,
        'Eb': 6,
        'E': 7,
        'F': 8,
        'F#': 9,
        'Gb': 9,
        'G': 10,
        'G#': 11,
        'Ab': 11,
    }

    REPLACE = {'H': 'B',
               'H#': 'C',
               'Hb': 'Bb',
               'Cb': 'B',
               'B#': 'C',
               'E#': 'F',
               'Fb': 'E'}

    def __init__(self, s, preference=None):
        """
        s is the string reprensenting the chord
        """

        for r in Chord.REPLACE:
            if r in s:
                s = s.replace(r, Chord.REPLACE[r])

        if preference is None:
            # prefer sharps unless the input is flat
            if 'b' in s:
                preference = 'b'
            else:
                preference = '#'

        self.set_preference(preference, refresh=False)

        self.key_stem = self.find_key_stem(s)
        self.modifier = self.find_modifier(s)
        self.bass = self.find_bass(s)

    def __repr__(self):
        repr_str = f'{self.key_stem}{self.modifier}'

        if self.bass is not None:
            repr_str += f'/{self.bass}'

        return repr_str

    def set_preference(self, preference, refresh=True):

        self.preference = preference
        inv_pref = list(set(('#', 'b')) - set(self.preference))[0]
        self.IDX_KEY_MAP = {}

        for k in Chord.KEY_IDX_MAP:

            if inv_pref in k:
                continue

            self.IDX_KEY_MAP[Chord.KEY_IDX_MAP[k]] = k

        if refresh:
            self.transpose(0)

    def transpose(self, steps=0):

        idx = Chord.KEY_IDX_MAP[self.key_stem]
        self.key_stem = self.IDX_KEY_MAP[(idx + steps) % 12]

        if self.bass is not None:
            idx = Chord.KEY_IDX_MAP[self.bass]
            self.bass = self.IDX_KEY_MAP[(idx + steps) % 12]

        return self

    def find_key_stem(self, s):

        s = s.strip()

        if len(s) > 1 and s[1] in ('#', 'b'):
            is_flat_sharp = True
        else:
            is_flat_sharp = False

        for n in [m for m in Chord.KEYS if len(m) == 1 + int(is_flat_sharp)]:
            for m in n:
                if s.startswith(m):
                    return m

        raise ValueError(f'Invalid chord string: {s}')

    def find_modifier(self, s):

        s = s.strip()

        # NOTE: need to check minor verison first
        # so that m7 is recognized before 7
        for mods in zip(Chord.MODIFIERS['minor'], Chord.MODIFIERS['major']):

            for m in mods:
                if m is None:
                    continue

                if m in s:
                    # don't use 2 different ways to represent minor chords
                    m = m.replace('min', 'm')

                    return m

        # if nothing matched there is no modifier at all
        return ''

    def find_bass(self, s):

        if s.count('/') == 1:
            bass = self.find_key_stem(s.split('/')[1])
        else:
            bass = None

        return bass
